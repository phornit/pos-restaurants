<?php
return[
    'welcome' => 'Welcome to the <b>Cambodia 4.0 Center</b> website',
    'welcome_user' => 'Welcome !',
    'logout' => 'Sign Out',
    'login' => 'Sign In',
    'register' => 'Register',
    'registrationForm' => 'Registration From',

    //Address
    'address' => '#36, 9th Floor, Street 169, Sangkat Veal Vong, Khan 7 Makara, Phnom Penh',

    //footer
    'subscribe' => 'Please SUBSCRIBE for the latest information and knowledge about technology and science',
    'contact' => 'Contact Us',
    'copyright' => '© 2020 Copyright Cambodia 4.0 Center',
]
?>
