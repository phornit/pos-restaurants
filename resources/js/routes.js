import Dashboard from './components/dashboard.vue'

import UserList from './components/auth/users/user-list.vue'
import UserAdd from './components/auth/users/user-add.vue'
import UserEdit from './components/auth/users/user-edit.vue'
import Profile from './components/auth/users/user-profile.vue'

import Audit from   './components/auth/Audit/audit-list'

import RoleList from './components/auth/roles/role-list.vue'

import file_upload from './components/corebackend/file-upload.vue'
import resizeImagePost from './components/corebackend/resizeImagePost.vue'

//Restaurant
import company from "./components/auth/company/company";
import table from "./components/backend/table/table-list"
import category from "./components/backend/category/category-list"
import product_size from "./components/backend/product_size/product-size"
import product from "./components/backend/product/product-list"

import pos from "./components/pos/pos"
import all_product from "./components/pos/all-product"
import table_frontend from "./components/pos/table"
import category_frontend from "./components/pos/product-category"
import product_by_category from "./components/pos/product-by-category"
import order_list_frontend from "./components/pos/order-list"
import receipt from "./components/pos/receipt"

export const routes = [
    //Backend
    {
        path: '/', component: pos,
        children: [
            { path: '/', component: table_frontend },
            { path: '/category-frontend', component: category_frontend },
            { path: '/all-product', component: all_product },
            { path: '/product-by-category/:category_id', component: product_by_category },
            { path: '/order-list-frontend', component: order_list_frontend },
            { path: '/receipt/:id', component: receipt },
        ]
    },
    {
        name:'dashboard',
        path:'/admin/dashboard',
        component:Dashboard
    },
    {
        name:'RoleList',
        path:'/admin/role-list',
        component:RoleList
    },
    {
        name:'UserList',
        path:'/admin/user-List',
        component:UserList
    },
    {
        name:'UserAdd',
        path:'/admin/user-add',
        component:UserAdd
    },
    {
        name:'Profile',
        path:'/admin/profile',
        component:Profile
    },
    {
        name:'UserEdit',
        path:'/admin/user-edit/:userid',
        component:UserEdit
    },
    {
        name:'Audit',
        path:'/admin/audit',
        component:Audit
    }
    ,{
        name:'file_upload',
        path:'/file_upload',
        component: file_upload
    }
    ,{
        name:'resizeImagePost',
        path:'/resizeImagePost',
        component: resizeImagePost
    },
    {
        name: 'company',
        path: '/admin/company',
        component: company
    },
    {
        name: 'table',
        path: '/admin/table',
        component: table
    },
    {
        name: 'category',
        path: '/admin/category',
        component: category
    },
    {
        name: 'product_size',
        path: '/admin/product-size',
        component: product_size
    },
    {
        name: 'product',
        path: '/admin/product',
        component: product
    }
];


