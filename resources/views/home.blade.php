@extends('layouts.frontend-template')

@section('content')
<div>
    <!-- =========home banner start============-->

    <div id="minimal-bootstrap-carousel" style="max-width: 1170px; margin: auto;" data-ride="carousel" class="layout-boxed carousel slide carousel-fade shop-slider full_width ver_new_1_slider">
        <!-- Wrapper for slides-->
        <div role="listbox" class="carousel-inner">
            <div style="background-image: url({{ asset('frontend/images/slider/homepage/1.jpg') }});" class="item active">
                <div class="carousel-caption">
                    <div class="thm-container">
                        <div class="box valign-top">

                        </div>
                    </div>
                </div>
            </div>
            <div style="background-image: url({{ asset('images/img5.jpg') }});" class="item">
                <div class="carousel-caption">
                    <div class="thm-container">
                        <div class="box valign-top">
                            <div class="content text-left pull-right">
                                <div style="text-align: center;"> <img src="{{ asset('images/logo_small.png') }}" style="width: 100px; margin: auto;"></div>
                                <h1 data-animation="animated fadeInLeft" class="bnrfnt40">ពាក្យស្នើសុំចូលជាសមាជិកមជ្ឈមណ្ឌលកម្ពុជា ៤.០</h1>
                                <p data-animation="animated fadeInRight" class="pln_he">មជ្ឈមណ្ឌលកម្ពុជា ៤.០ ត្រូវបានបង្កើតឡើងក្នុងបុព្វហេតុចូលរួមជាមួយរាជរដ្ឋាភិបាល<br/> និងភាគីពាក់ព័ន្ធក្នុងការកសាងសេដ្ឋកិច្ចឌីជីថលនៅកម្ពុជា.</p>
                                <div class="req-button-primary text-left">
                                    <a href="{{url('member')}}" class="submit">ពាក្យសុំចូលជាសមាជិក</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="background-image: url({{ asset('images/img6.jpg') }});" class="item">
                <div class="carousel-caption">
                    <div class="thm-container">
                        <div class="box valign-top">
                            <div class="content text-left pull-right">
                                <div style="text-align: center;"> <img src="{{ asset('images/logo_small3.png') }}" style="width: 100px; margin: auto;"></div>
                                <h1 data-animation="animated fadeInUp" class="bnrfnt40">ពាក្យសុំចូលជាសមាជិក​ របស់ក្លឹបអ្នកជំនាញបច្ចេកវិទ្យាឌីជីថល និងអ្នកវិទ្យាសាស្រ្ត</h1>
                                <p data-animation="animated fadeInDown" class="pln_he">ក្លឹបអ្នកជំនាញបច្ចេកវិទ្យាឌីជីថល និងអ្នកវិទ្យាសាស្រ្ត (ក.ប.វ.) ជាក្លឹបដែលប្រមូលផ្តុំដោយសាស្រ្តាចារ្យ អ្នកស្រាវជ្រាវ អ្នកបច្ចេកទេស អ្នកជំនាញ និងអ្នក មានចំណេះដឹងផ្នែកវិទ្យាសាស្រ្ត និងផ្នែកបច្ចេកវិទ្យា</p><div class="req-button-primary text-left"><a href="http://127.0.0.1:8000/member" class="submit">ពាក្យសុំចូលជាសមាជិក</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="background-image: url({{ asset('images/img7.jpg') }});" class="item">
                <div class="carousel-caption">
                    <div class="thm-container">
                        <div class="box valign-top">
                            <div class="content text-left pull-right">
                                <div style="text-align: center;"> <img src="{{ asset('images/logo_small4.png') }}" style="width: 100px; margin: auto;"></div>
                                <h1 data-animation="animated fadeInUp" class="bnrfnt40">ពាក្យសុំចូលជាសមាជិក ក្លឹបធុរកិច្ចថ្មីកម្ពុជា</h1>
                                <p data-animation="animated fadeInDown" class="pln_he">ក្លឹបនេះត្រូវបានបង្កើតនៅក្រោមឆ័ត្រនៃ “មជ្ឈមណ្ឌលកម្ពុជា៤.០” ដើម្បីផ្ដល់ការគាំទ្រដល់ ស្ថាបនិកនៃធុរកិច្ចថ្មីនានា ដែលមានគោលដៅចូលរួមអភិវឌ្ឍសេដ្ឋកិច្ចឌីជីថលកម្ពុជា</p><div class="req-button-primary text-left"><a href="http://127.0.0.1:8000/member" class="submit">ពាក្យសុំចូលជាសមាជិក</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="background-image: url({{ asset('images/img10.jpg') }});" class="item">
                <div class="carousel-caption">
                    <div class="thm-container">
                        <div class="box valign-top">
                            <div class="content text-left pull-right">
                                <div style="text-align: center;"> <img src="{{ asset('images/logo_small5.png') }}" style="width: 100px; margin: auto;"></div>
                                <h1 data-animation="animated fadeInUp" class="bnrfnt40">ពាក្យសុំចូលជាសមាជិក​ ក្លឹបអ្នកគណិតវិទ្យា និងអ្នកវិទ្យាសាស្រ្តវ័យក្មេង (ក.គ.វ)</h1>
                                <p data-animation="animated fadeInDown" class="pln_he">ក្លឹបអ្នកគណិតវិទ្យា និងអ្នកវិទ្យាសាស្ត្រវ័យក្មេង គឺជាក្លឹបដែលចំណុះឱ្យ មជ្ឈមណ្ឌលកម្ពុជា៤.០ ដើម្បីប្រមូលផ្តុំដោយអ្នកគណិតវិទ្យា និងអ្នកវិទ្យាសាស្ត្រវ័យក្មេង</p><div class="req-button-primary text-left"><a href="http://127.0.0.1:8000/member" class="submit">ពាក្យសុំចូលជាសមាជិក</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Controls--><a href="#minimal-bootstrap-carousel" role="button" data-slide="prev" class="left carousel-control"><i class="fa fa-angle-left"></i><span class="sr-only">Previous</span></a><a href="#minimal-bootstrap-carousel" role="button" data-slide="next" class="right carousel-control"><i class="fa fa-angle-right"></i><span class="sr-only">Next</span></a>
    </div>


    <!-- =========home banner end============-->
    <!-- We offer Different Services-->
    <section class="container layout-boxed diff-offer-wrapper">
        <div>
            <div class="row diff-offer" style="display: block;">
                <ul>
                    <li class="we-offer-cont">
                        <h2>មជ្ឈមណ្ឌលកម្ពុជា ៤.០<span>Cambodia 4.0 Center</span></h2>
                    </li>

                    <div class="req-button text-right mobile-show req-button-3" style="padding-top: 5px; clear: both;">
                        <a href="{{url('member')}}" class="submit">ពាក្យសុំចូលជាសមាជិក</a>
                    </div>

                    <li class="we-offer-cont2">
                        <p>មជ្ឈមណ្ឌលកម្ពុជា ៤.០ ត្រូវបានបង្កើតឡើងក្នុងបុព្វហេតុចូលរួមជាមួយរាជរដ្ឋាភិបាល និងភាគីពាក់ព័ន្ធក្នុងការកសាងសេដ្ឋកិច្ចឌីជីថលនៅកម្ពុជា តាមរយៈការបណ្តុះបណ្តាល និងតម្រង់ទិសផ្នែកជំនាញឌីជីថល និងធុរកិច្ចថ្មី (Startups) ផ្អែកលើថ្នាលបច្ចេកវិទ្យា...</p>
                    </li>
                </ul>
            </div>

            <div class="section_header2 common" style="margin-top: 30px;">
                <h2>ពីនេះពីនោះ</h2>
            </div>

            <div class="row">
                @foreach ($content as $con)
                    <div class="col-sm-3 service-info">
                        <div class="item">
                            <a href="{{ url('/'.$con->slug) }}" class="post-image view image_hover">
                                <img src="{{asset('images/thumbnails')}}/{{$con->thumb_image}}" alt="" class="img-responsive zoom_img_effect">
                            </a>
                            <a href="{{ url('/'.$con->slug) }}">
                                <h4><div class="title-height">{{ $con->title }}</div> </h4>
                            </a>
                            <p>{!! $con->brief !!}...</p>
                            <h6><a href="{{ url('/'.$con->slug) }}">អានបន្ត</a></h6>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- We offer Different Services-->


    <section class="diff-offer-wrapper diff-offer-wrapper3 container clearfix ind-common-pad" style="background-color: white;">
        <div class="container">
            <div class="diff-offer">
                <div class="section_header2 common">
                    <h2><a href="/subject_frontend_list" style="text-decoration: none; color: black;">កម្មវិធីអក្ខរកម្មឌីជីថល (E-Learning)</a></h2>
                </div>
            </div>


            <div class="row">

                @for ($i = 0; $i < count($subject); $i++)

                @if($i < 2)
                <div class="col-md-4 col-sm-6 service-info">
                    <div class="item">
                        <a href="{{ url('/subject_frontend_detail/'.$subject[$i]->id) }}">
                        <div class="post-image view image_hover">
                            <img src="{{asset('/images/thumbnails/')}}/{{$subject[$i]->thumbnail}}" alt="" class="img-responsive zoom_img_effect" style="height: 202px;">
                        </div>
                        </a>
                        <h4><a href="{{ url('/subject_frontend_detail/'.$subject[$i]->id) }}">{{$subject[$i]->title}}</a></h4>

                        <p style="height: 48px; line-height: 24px;">{!! $subject[$i]->description !!}...</p>

                        <h6><a href="{{ url('/subject_frontend_detail/'.$subject[$i]->id) }}">អានបន្ត</a></h6>
                    </div>
                </div>
                @endif

                @if($i > 1)
                <div class="col-md-4 col-sm-12 service-info service-info-right">
                    <div class="item">

                        <div class="post-image view image_hover">
                            <a href="{{ url('/subject_frontend_detail/'.$subject[$i]->id) }}">
                            <img src="{{asset('/images/thumbnails/')}}/{{$subject[$i]->thumbnail}}" alt="" class="img-responsive zoom_img_effect">
                            </a>
                        </div>

                        <div class="post-content">
                            <h4> <a href="{{ url('/subject_frontend_detail/'.$subject[$i]->id) }}" style="line-height: 22px;">{{$subject[$i]->title}}</a></h4>
                            <p style="height: 44px; line-height: 22px; margin-top: 5px;">{!! $subject[$i]->description !!}...<a href="{{ url('/subject_frontend_detail/'.$subject[$i]->id) }}">អានបន្ត</a></p>
                        </div>
                    </div>
                </div>
                @endif

                @endfor
            </div>
        </div>
    </section>


    <!-- Control in compliance-->
    <section class="container ind-common-pad2 latest-news1 sectpad">
        <div class="container clearfix">
            <div class="row">
                <div class="col-lg-7 about-sec-content">
                    <div class="section_header2 common">
                        <h2>មជ្ឈមណ្ឌលកម្ពុជា ៤.០</h2>
                    </div>
                    <p>ត្រូវបង្កើតឡើងក្នុងគោលបំណង ដើម្បីចូលរួមជាមួយរាជរដ្ឋាភិបាល និងភាគីពាក់ព័ន្ធ ក្នុងការកសាងសេដ្ឋកិច្ចឌីជីថលនៅកម្ពុជា ពិសេសដាក់ចេញយុទ្ធនាការ “កម្ពុជា ៤.០” ដោយអនុលោមតាមគោលការណ៍គ្រឹះចំនួន៥ ដូចខាងក្រោម៖</p>
                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                <li><i class="fa fa-arrow-circle-right"></i> គោលការណ៍គ្រឹះទី១៖ រួមគ្នាកសាងហេដ្ឋារចនាសម្ព័ន្ធ និងប្រព័ន្ធ Ecosystemនៃឧស្សាហកម្ម ៤.០</li>
                                <li><i class="fa fa-arrow-circle-right"></i> គោលការណ៍គ្រឹះទី២៖ រួមគ្នាអភិវឌ្ឍន៍ធនធានមនុស្ស និងកំលាំងពលកម្មលើផ្នែកឌីជីថល</li>
                                <li><i class="fa fa-arrow-circle-right"></i> គោលការណ៍គ្រឹះទី៣៖ រួមគ្នាជំរុញអភិវឌ្ឍន៍បច្ចេកវិទ្យាឌីជីថល</li>
                                <li><i class="fa fa-arrow-circle-right"></i> គោលការណ៍គ្រឹះទី៤៖ រួមគ្នាជំរុញអភិវឌ្ឍន៍រដ្ឋាភិបាលឌីជីថល</li>
                                <li><i class="fa fa-arrow-circle-right"></i> គោលការណ៍គ្រឹះទី៥៖ រួមគ្នាពង្រឹងសន្តិសុខតាមប្រព័ន្ធអ៊ីនធឺណែត (Cyber-Security)</li>
                            </ul>
                        </div>
                    </div>
                    <br/>
                    <p>
                        “កម្ពុជា ៤.០” ផ្តោតលើអភិក្រមចំនួន៣ (3cs): Curriculum Campaign and Contest សំដៅចូលរួមគាំទ្រជាមួយរាជរដ្ឋាភិបាលក្នុងការជម្រុញការកសាងសេដ្ឋកិច្ចឌីជីថលនៅកម្ពុជា សំដៅសម្រេចបានចក្ខុវិស័យក្នុងការប្រែក្លាយ កម្ពុជាទៅជាប្រទេសដែលមានចំណូលមធ្យមកម្រិតខ្ពស់ នៅឆ្នាំ២០៣០ និងជាប្រទេសមានចំណូលខ្ពស់នៅឆ្នាំ២០៥០។
                    </p>
                </div>
                <div class="col-lg-4">
                    <div class="section-faq">
                        <div class="section_header2 common">

                        </div>
                        <div class="accordian-area accordian-area-pad">
                            <br/>
                            <img class="img-fluid w-100 u-shadow-v21 rounded" src="http://cambodia4point0.org/images/900.jpg?1" alt="ចាប់យកបច្ចេកវិទ្យាឌីជីថលសម្រាប់ជីវិតកាន់តែប្រសើរ" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Control in compliance-->
{{--    <div class="container clearfix ind-common-pad">--}}
{{--        <div class="row">--}}
{{--            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 our-t-client">--}}
{{--                <div class="section_header2 common">--}}
{{--                    <h2>ដៃគូសហការ</h2>--}}
{{--                </div>--}}
{{--                <p></p>--}}
{{--                <ul class="clearfix">--}}
{{--                    <li><img src="{{ asset('frontend/images/clients/7.jpg')}}" alt="" class="img-responsive"></li>--}}
{{--                    <li><img src="{{ asset('frontend/images/clients/8.jpg')}}" alt="" class="img-responsive"></li>--}}
{{--                    <li><img src="{{ asset('frontend/images/clients/9.jpg')}}" alt="" class="img-responsive"></li>--}}
{{--                    <li><img src="{{ asset('frontend/images/clients/10.jpg')}}" alt="" class="img-responsive"></li>--}}
{{--                    <li><img src="{{ asset('frontend/images/clients/11.jpg')}}" alt="" class="img-responsive"></li>--}}
{{--                    <li><img src="{{ asset('frontend/images/clients/12.jpg')}}" alt="" class="img-responsive"></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- Indurial Solution-->
    <section class="indurial-t-solution indurial-solution indpad anim-5-all indurial-t-solution3">
        <div class="container clearfix">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="indurial-solution-text text-center">
                        <h2>{{__('msg.subscribe')}}</h2><span class="contactus-button2 text-center"><a href="/page/contact-us" class="submit">Contact Us </a></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Indurial Solution-->
</div>
@endsection
