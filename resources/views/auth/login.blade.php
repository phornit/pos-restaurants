
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cambodia 4.0 Center | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/app.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css" />

    <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">

    <style lang="scss">
        a {
            color: rgb(0, 0, 0);
        }
        @font-face {
            font-family: "Odor Mean Chey";
            src: url('/fonts/khmer-font/OdorMeanChey.ttf');
            font-weight: 400;
            font-style: normal;
        }
        body {
            font: 100 0.8em/0.8em "Odor Mean Chey", sans-serif;
            font-weight: normal;
            height: 100%;
            overflow-x: hidden;
            letter-spacing: 0.2px;
            line-height: 20px;
        }
        .login-page,
        .register-page {
            -ms-flex-align: center;
            align-items: center;
            background: #0c233a;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            height: 70vh;
            -ms-flex-pack: center;
            justify-content: center;
        }
        ...
    </style>

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <img src="./img/logo.png" alt="AdminLTE Logo" style="width: 100px;">
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <h3><p class="login-box-msg">ចូលប្រើប្រាស់ប្រព័ន្ធ</p></h3>

            <form action="{{ url('/authenticate') }}" method="post">
                @csrf
                <input type="hidden" id="url" name="url" value="{!! $url !!}">
                <div class="input-group mb-3">
                    <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required  autofocus>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>

                    @error('emails')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>
                <div class="input-group mb-3">
                    <input type="password" id="password" name="password" class="form-control" value="{{$password}}" placeholder="ពាក្យសម្ងាត់" @error('password') is-invalid @enderror required autocomplete="current-password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                </div>
                <div class="row">
                    <div class="col-8" style="max-width: 62.6666666667%;">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember" name="remember" checked="{{$remember}}">
                            <label for="remember">
                                ចង់ចាំពាក្យសម្ងាត់
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block" style="width: 100px; font-size: 12px;">ចូលប្រើប្រាស់</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <br/>

            <p class="mb-1">
                <a href="/password/reset" style="font-size: 12px; color: #6d7a86;">ខ្ញុំភ្លេចពាក្យសម្ងាត់របស់ខ្ញុំ</a>
            </p>
            <p class="mb-0">
                <a href="/register" class="text-center" style="font-size: 12px; color: #6d7a86;">ចុះឈ្មោះសមាជិកថ្មី</a> <br/>
            </p>
        </div>
        <!-- /.login-card-body -->


        @if(Session::has('message'))
            {!! Session::get('message') !!}
        @endif

    </div>
</div>

@auth
    <script>
        window.user = @json(auth()->user())
    </script>
@endauth
<script src="/js/app.js"></script>
<script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>

</body>
</html>
