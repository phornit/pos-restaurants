@extends('layouts.frontend-template')

@section('content')
    <section class="inner-banner2 clearfix">
        <div class="container clearfix">
            <h2>ចុះឈ្មោះ</h2>
        </div>
    </section>

    <section class="breadcumb-wrapper">
        <div class="container clearfix">
            <ul class="breadcumb">
                <li><a href="/">ទំព័រដើម</a></li>
                <li><span>ចុះឈ្មោះ</span></li>
            </ul>
        </div>
    </section>

    <div class="container" style="padding-bottom: 50px;">

        <div class="row">
            <div class="col-md-6">
                <div class="diff-offer">
                    <div class="section_header2 common">
                        <h2>ចុះឈ្មោះ</h2>
                    </div>
                </div>

                <form method="POST" action="{{url('userRegister')}}">
                    @csrf
                <div class="form-group">
                    <label for="name" class="col-md-3 col-form-label text-md-right">{{ __('ឈ្មោះ') }}</label>
                    <div class="col-md-10">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <br style="clear: both;"/>
                </div>

                <div class="form-group" style="margin-top: 10px; clear: both;">
                    <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('អ៊ីម៉ែល​(E-Mail)') }}</label>
                    <div class="col-md-10">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <br style="clear: both;"/>
                </div>


                <div class="form-group" style="margin-top: 10px; clear: both;">
                    <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('ពាក្យសម្ងាត់') }}</label>
                    <div class="col-md-10">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <br style="clear: both;"/>
                </div>

                <div class="form-group" style="margin-top: 10px; clear: both;">
                        <label for="password-confirm" class="col-md-3 col-form-label text-md-right">{{ __('បញ្ជាក់ពាក្យសម្ងាត់') }}</label>
                        <div class="col-md-10">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                        <br style="clear: both;"/>
                    </div>


                    <div class="form-group" style="margin-top: 10px; clear: both;">
                        <label for="password-confirm" class="col-md-3 col-form-label text-md-right"></label>
                        <div class="col-md-10">
                            @if(Session::has('message'))
                                {!! Session::get('message') !!}
                            @endif
                        </div>
                        <br style="clear: both;"/>
                    </div>


                <div class="form-group row mb-0" style="margin-left: 0px;">
                    <hr/>
                    <label for="password-confirm" class="col-md-3 col-form-label text-md-right"></label>
                    <div class="col-md-10 offset-md-3">
                        <button type="submit" class="btn btn-primary" style="padding: 10px 30px;">
                            {{ __('ចុះឈ្មោះ') }}
                        </button>
                    </div>
                </div>

                </form>

            </div>
            <div class="col-md-6">
                <div class="diff-offer">
                    <div class="section_header2 common">
                        <h2 style="line-height: 30px;">របៀបចុះឈ្មោះចូលរៀនកម្មវិធីអក្ខរកម្មឌីជីថល</h2>
                    </div>
                </div>
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/Z0ZVuil_7YU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>

@endsection
