<!DOCTYPE html>
<html>
<head>
    <title>Cambodia 4.0 Center</title>
</head>
<body>
<h3>សួស្តី {{$data['name']}},</h3>
<br/>
<p>សូមអរគុណសម្រាប់ការចុះឈ្មោះជាមួយនឹងគេហទំព័រ <a href="http://cambodia4point0.org">www.cambodia4point0.org</a></p>
<p>សូមចុចតំណរភ្ជាប់នេះ <a href="http://cambodia4point0.org/verify-account/{{$data['id']}}">Verify Account</a> ដើម្បីដំណើរគណនីរបស់អ្នក</p>
<br/>
សូមអរគុណ!<br/>
លេខទូរស័ព្ទ : +855 12 628 665<br/>
អ៊ីមែល : info@cambodia4point0.org<br/>
គេហទំព័រ :<a href="www.cambodia4point0.org">www.cambodia4point0.org </a>
</body>
</html>
