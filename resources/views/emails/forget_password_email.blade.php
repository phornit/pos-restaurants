<!DOCTYPE html>
<html>
<head>
    <title>Cambodia 4.0 Center</title>
    <link rel="stylesheet" href="http://cambodia4point0.org/css/app.css">
    <style>
        a{
            text-decoration: none;
            color: white;
        }
        .reset_password {
            background: #3c8dbc;
            color: #fff;
            font: 400 1.08em/1em 'Odor Mean Chey', sans-serif;
            display: inline-block;
            padding: 1em 1.52em;
            border-radius: 4px;
            border: 1px solid #3c8dbc;
            margin: 0;
        }
    </style>
</head>
<body>
<div style="width: 100%; background-color: #e6e8eb; padding: 50px;">
<img src="http://cambodia4point0.org/frontend/images/header/logo-2.png">
<br/>
<br/>
<h3>Welcome {{$data['name']}},</h3>
<br/>
<p>Use the link below to set up a new password for your account.</p>
<a href="{{$data['link']}}" class="reset_password" style="color: white;">Reset My Password</a>
<br/>
<br/>
<p> If you did not forgot your password you can safely ignore this email and the link will expire on its own.</p>
<br/>
Thanks!<br/>
Phone : +855 12 628 665<br/>
Email : info@cambodia4point0.org<br/>
Website :<a href="www.cambodia4point0.org">www.cambodia4point0.org </a>
</div>
</body>
</html>
