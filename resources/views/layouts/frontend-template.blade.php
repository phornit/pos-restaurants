<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="user" content="{{ optional(Auth::user())->id }}">
    <title>POS Restaurant | Content Management System (CMS)</title>

    <link rel="shortcut icon" href="/images/logo_small.png">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css" />

    <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">

    <style lang="scss">
        a {
            color: rgb(0, 0, 0);
        }

        input::-webkit-input-placeholder {
            color: #0000004f !important;
        }

        input:-moz-placeholder { /* Firefox 18- */
            color: #0000004f !important;
        }

        input::-moz-placeholder {  /* Firefox 19+ */
            color: #0000004f !important;
        }

        input:-ms-input-placeholder {
            color: #0000004f !important;
        }
        body {
            overflow-x: hidden;
        }
        .smaller{
            width:100%;
            height:23px;
            background-color:rgba(0,0,0,0.6);
            position:absolute;
            top:78px;
            text-align:center;
            color:white;
            padding: 3px;
            font-size: 12px;
        }
    </style>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

    <nav class="main-header navbar navbar-expand navbar-dark navbar-navy" style="margin-left: 450px; height: 60px;">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item" style="color: white;">
{{--                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>--}}
                ស្វែងរក :
            </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
                <input class="form-control" @keyup="searchit" v-model="search" style="width: 150px;" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" @click="searchit" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">

            <li class="nav-item d-none d-sm-inline-block" style="margin-right: 5px;">
                <router-link to="/" class="btn btn-info pos-btn">
                    <img src="/img/table-icon.png" class="table-icon"><span class="btn-title"> ពត៌មានតុ</span>
                </router-link>
            </li>

            <li class="nav-item d-none d-sm-inline-block" style="margin-right: 5px;">
                <router-link to="/category-frontend" class="btn btn-primary pos-btn">
                    <i class="fas fa-sitemap pos-icon"></i> <span class="btn-title"> ប្រភេទទំនិញ</span>
                </router-link>
            </li>

            <li class="nav-item d-none d-sm-inline-block" style="margin-right: 5px;">
                <router-link to="/all-product" class="btn btn-success pos-btn">
                    <i class="fa fa-utensils pos-icon"></i><span class="btn-title"> បង្ហាញទំនិញ</span>
                </router-link>
            </li>

            <li class="nav-item d-none d-sm-inline-block"  style="margin-right: 5px;">
                <router-link to="/order-list-frontend" class="btn btn-secondary pos-btn">
                    <i class="far fa-edit pos-icon"></i><span class="btn-title"> ការកម្មង់</span>
                </router-link>
            </li>
            @if(Auth::user()->user_role < 2)
            <li class="nav-item d-none d-sm-inline-block" style="margin-right: 5px;">
                <a href="/admin/dashboard" class="btn btn-warning pos-btn">
                    <i class="fa fa-cog pos-icon"></i><span class="btn-title"> សារពើភ័ណ្ឌ</span>
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a class="btn btn-danger btn-sm logout-btn" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off logout-icon"></i>
                </a>
            </li>
        </ul>
    </nav>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>

    <router-view></router-view>

    <!-- Main Footer -->
    <footer class="main-footer" style="margin-left: 450px;">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
                អត្រាប្តូរប្រាក់ : ៛{{$DefaultExchangeRate}}
        </div>
        <!-- Default to the left -->
        <span>V.1.0.0</span>  អ្នកប្រើប្រាស់ :​<span style="text-transform: uppercase;"> {{ Auth::user()->name }}</span>
    </footer>
</div>
<!-- ./wrapper -->

@auth
    <script>
        window.user = @json(auth()->user())
    </script>
@endauth

<script type="text/javascript">
    window.onload = function() {
        var height = getViewportHeight() - 60;
        if(height > 0)
            document.getElementById("test").style.height = height + "px";
    }
    function getViewportHeight() {
        var h = 0;
        if(self.innerHeight)
            h = window.innerHeight;
        else if(document.documentElement && document.documentElement.clientHeight)
            h = document.documentElement.clientHeight;
        else if(document.body)
            h = document.body.clientHeight;
        return h;
    }
</script>

<script src="/js/app.js"></script>
<script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
</body>
</html>
