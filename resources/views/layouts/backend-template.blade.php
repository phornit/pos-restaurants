<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="user" content="{{ optional(Auth::user())->id }}">
  <title>POS Restaurant | Content Management System (CMS)</title>

    <link rel="shortcut icon" href="/images/logo_small.png">

  <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css" />

    <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
    <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">

    <style lang="scss">
        a {
            color: rgb(0, 0, 0);
        }
        @font-face {
            font-family: "Odor Mean Chey";

            src: url('/fonts/khmer-font/KhmerOSContent-Bold.ttf');
            src: url('/fonts/khmer-font/KhmerOSContent-Regular.ttf');
            src: url('/fonts/khmer-font/OdorMeanChey.ttf');
            font-weight: 400;
            font-style: normal;
        }
        input::-webkit-input-placeholder {
            color: #0000004f !important;
        }

        input:-moz-placeholder { /* Firefox 18- */
            color: #0000004f !important;
        }

        input::-moz-placeholder {  /* Firefox 19+ */
            color: #0000004f !important;
        }

        input:-ms-input-placeholder {
            color: #0000004f !important;
        }
        ...
    </style>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">

    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" @keyup="searchit" v-model="search" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" @click="searchit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
  </nav>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/admin/dashboard" class="brand-link">
      <img src="/img/logo.png" alt="AdminLTE Logo" style="width: 43px;">
      <span class="brand-text font-weight-light">សួនឬស្សី</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/img/profile.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block">
                {{Auth::user()->name}}
                {{--<p>{{Auth::user()->type}}</p>--}}
            </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

            {!! $backend_menu !!}


          <li class="nav-item">
                <router-link to="/admin/profile" class="nav-link">
                    <i class="nav-icon fas fa-user orange"></i>
                    <p>
                        ប្រវត្តិរូប
                    </p>
                </router-link>
         </li>

          <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                    <i class="nav-icon fa fa-power-off red"></i>
                    <p>
                        ចាកចេញ
                    </p>
                 </a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form>
        </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

    <br/>

  <div class="content-wrapper">
    <router-view></router-view>
    <vue-progress-bar></vue-progress-bar>
    <!-- /.content -->
  </div>



    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
            អត្រាប្តូរប្រាក់ : ៛{{$DefaultExchangeRate}}
        </div>
        <!-- Default to the left -->
        <span>V.1.0.0</span>  អ្នកប្រើប្រាស់ :​<span style="text-transform: uppercase;"> {{ Auth::user()->name }}</span>
    </footer>
</div>
<!-- ./wrapper -->

@auth
<script>
    window.user = @json(auth()->user())
</script>
@endauth
<script src="/js/app.js"></script>
<script src="https://ckeditor.com/apps/ckfinder/3.4.5/ckfinder.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
</body>
</html>
