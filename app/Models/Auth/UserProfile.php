<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UserProfile extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'core_user_details';
    public $timestamps = false;

    protected $fillable = [
        'user_account_id','name_kh', 'name_en', 'sex', 'fax', 'nationality', 'date_of_birth', 'place_of_birth', 'phone', 'address', 'photo'
    ];
}
