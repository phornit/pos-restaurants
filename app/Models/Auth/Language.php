<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Language extends Model
{
    protected $table = 'core_languages';

    protected $fillable = [
        'id','name','trans_name', 'state','localization', 'script', 'image', 'ordering'
    ];

    public static function getListLanguage(){
        $query = DB::table('core_languages');
        $query->where('core_languages.state', '=', 1);
        $list_language = $query->get();

        return $list_language;
    }

    public static function getLanguageActive(){
        $query = DB::table('core_languages');
        $query->where('state', 1);
        $results = $query->get();
        if ($results)
        {
            return $results;
        }
    }
}
