<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    //
    protected $table = "core_user_permission";
    public $timestamps = false;

    protected $fillable = [
        'user_roles_id','menu_id'
    ];
}
