<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class UserAccount extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'core_users_account';

    protected $fillable = [
        'id','name', 'email', 'password', 'user_role', 'state', 'trashed', 'trashed_by', 'trashed_at'
    ];
}
