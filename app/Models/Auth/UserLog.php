<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $table="core_user_log";
    public $timestamps = false;

    protected $fillable = [
        'user_id','ip_address','browser_info','login_date','logout_date','isLogin'
    ];

    public static function Login($user_id){
        $ip_address = request()->ip();
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $day = date('Y-m-d H:i:s');
        UserLog::create([
            'user_id' => $user_id,
            'ip_address' => $ip_address,
            'browser_info' => $browser,
            'login_date' => $day,
            'isLogin' => 1
        ]);
    }

    public static function Logout($user_id){
        $ip_address = request()->ip();
        $day = date('Y-m-d H:i:s');
        UserLog::where('user_id',$user_id)
            ->where('isLogin',1)
            ->where('ip_address', $ip_address)
            ->update([
            'logout_date' => $day,
            'isLogin' => 0
        ]);
    }

}
