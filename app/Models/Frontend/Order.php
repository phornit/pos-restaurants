<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'tbl_order';

    protected $fillable = [
        'id',
        'order_number',
        'currency_id',
        'table_id',
        'table_name',
        'order_status',
        'order_type',
        'remark',
        'fee_amount',
        'service_charge',
        'discount_percent',
        'discount_amount',
        'tax_percent',
        'tax_amount',
        'net_amount',
        'total_amount',
        'created_by',
        'updated_by'
    ];
}
