<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'tbl_order_detail';

    protected $fillable = [
        'order_id',
        'product_id',
        'product_name',
        'product_size_id',
        'size_name',
        'qty',
        'dis_percent',
        'vat_percent',
        'unit_price',
        'is_void',
        'amount',
        'is_free',
        'remak',
    ];
}
