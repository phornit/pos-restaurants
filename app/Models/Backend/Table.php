<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $table = 'tbl_table';

    protected $fillable = [
        'id','name','description','isAvailable'
    ];

    public static function updateStatusToBusy($id){
        Table::where('id', $id)->update([
            'isAvailable' => 0,
        ]);
    }

    public static function updateStatusToAvailable($id){
        Table::where('id', $id)->update([
            'isAvailable' => 1,
        ]);
    }
}
