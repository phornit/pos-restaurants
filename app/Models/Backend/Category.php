<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'tbl_category';

    protected $fillable = [
        'cat_name','cat_description', 'cate_image'
    ];
}
