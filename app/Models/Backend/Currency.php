<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'tbl_currency';

    protected $fillable = [
        'name','abbr','symbol','description','isDefault'
    ];

}
