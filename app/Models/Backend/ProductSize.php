<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
    protected $table = 'tbl_product_size';

    protected $fillable = [
        'size_name','size_description'
    ];
}
