<?php

namespace App\Models\Backend;

use App\Http\Resources\productPriceResource;
use App\Http\Resources\sendResource;
use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $table = 'tbl_product_price';

    protected $fillable = [
        'id','product_id','product_size_id', 'price', 'cost'
    ];

}
