<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'tbl_company';
    public $timestamps = false;

    protected $fillable = [
        'name_kh','name_en', 'description', 'phone', 'email', 'address1', 'address2', 'facebook', 'logo'
    ];
}
