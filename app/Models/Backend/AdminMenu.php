<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class AdminMenu extends Model
{
    protected $table = "core_backend_menus";

    protected $fillable = [
        'name',
        'parent_id',
        'link',
        'class_name',
        'color',
        'state',
    ];
}
