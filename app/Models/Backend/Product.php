<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'tbl_product';

    protected $fillable = [
        'code','category_id','name', 'description','photo'
    ];
}
