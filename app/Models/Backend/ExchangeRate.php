<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    protected $table = 'tbl_exchange_rate';
    public $timestamps = false;

    protected $fillable = [
        'exchange_rate','description','from_currency','to_currency'
    ];
}
