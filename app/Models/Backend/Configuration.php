<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $table = 'configuration_info';

    protected $fillable = [
        'name','value','is_active'
    ];
}
