<?php

namespace App\Http\Resources;

use App\Models\Backend\AdminMenu;
use Illuminate\Http\Resources\Json\ResourceCollection;

class mainMenuCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'parent_id' => $page->parent_id,
                    'name' => $page->name,
                    'link' => $page->link,
                    'class_name' => $page->class_name,
                    'state' => $page->state,
                    'color' => $page->color,
                    'parent_name' => AdminMenu::find($page->parent_id)
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
