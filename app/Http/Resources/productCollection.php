<?php

namespace App\Http\Resources;

use App\Models\Backend\Category;
use App\Models\Backend\ProductPrice;
use App\Models\Backend\ProductSize;
use Illuminate\Http\Resources\Json\ResourceCollection;

class productCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'code' => $page->code,
                    'category_id' => $page->category_id,
                    'category_name' => Category::where('id',$page->category_id)->pluck('cat_name')->first(),
                    'name' => $page->name,
                    'description' => $page->description,
                    'photo' => $page->photo,
                    'product_price' => $this->getProductPrice($page->id),
                ];
            }),
        ];
    }

    public function getProductPrice($product_id){
        $productPrice = ProductPrice::join('tbl_product_size', 'tbl_product_price.product_size_id', '=', 'tbl_product_size.id')
            ->select('tbl_product_size.*','tbl_product_price.*')
            ->where('product_id', $product_id)->get();
        return $productPrice;
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
