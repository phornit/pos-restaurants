<?php

namespace App\Http\Resources;

use App\Models\Auth\UserAccount;
use App\Models\Frontend\OrderDetail;
use Illuminate\Http\Resources\Json\JsonResource;

class orderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_number' => $this->order_number,
            'currency_id' => $this->currency_id,
            'table_id' => $this->table_id,
            'table_name' => $this->table_name,
            'order_status' => $this->order_status,
            'order_type' => $this->order_type,
            'remark' => $this->remark,
            'fee_amount' => $this->fee_amount,
            'service_charge' => $this->service_charge,
            'discount_percent' => $this->discount_percent,
            'discount_amount' => $this->discount_amount,
            'tax_amount' => $this->tax_amount,
            'net_amount' => $this->net_amount,
            'total_amount' => $this->total_amount,
            'created_at' => $this->created_at,
            'created_by' => UserAccount::where('id',$this->created_by)->pluck('name')->first(),
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'product' => OrderDetail::where('order_id', $this->id)->get(),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
