<?php

namespace App\Http\Resources;

use App\Models\Frontend\Menu;
use Illuminate\Http\Resources\Json\ResourceCollection;

class menuCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'parent_id' => $page->parent_id,
                    'parent_sub_id' => $page->parent_sub_id,
                    'name' => $page->name,
                    'alias' => $page->alias,
                    'type' => $page->type,
                    'link_href' => $page->link_href,
                    'link_cat_id' => $page->link_cat_id,
                    'link_catsub_id' => $page->link_catsub_id,
                    'link_cont_id' => $page->link_cont_id,
                    'link_mod_id' => $page->link_mod_id,
                    'target' => $page->target,
                    'ordering' => $page->ordering,
                    'state' => $page->state,
                    'parent' => Menu::where('parent_id', $page->id)->where('trashed',0)->get()
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
