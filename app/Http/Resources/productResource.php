<?php

namespace App\Http\Resources;

use App\Models\Backend\Category;
use App\Models\Backend\ProductPrice;
use Illuminate\Http\Resources\Json\JsonResource;

class productResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'category_id' => $this->category_id,
            'category_name' => Category::where('id',$this->category_id)->pluck('cat_name')->first(),
            'name' => $this->name,
            'description' => $this->description,
            'photo' => $this->photo,
            'product_price' => ProductPrice::where('product_id', $this->id)->get()
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
