<?php

namespace App\Http\Middleware;

use App\Modules\Frontend\Utilities;
use Closure;

class Utility
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share( 'DefaultExchangeRate', Utilities::DefaultExchangeRate());
        return $next($request);
    }
}
