<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use CKSource\CKFinder\Image;

class FileUploadController extends Controller
{
    public function fileUpload(Request $request) {
        $this->validate($request, [
            'myFile' => $request['validate'],
        ]);

        $image = $request->file('myFile');
        $imageName = time().'.'.$image->extension();

        $newImageName = date('Y-m-d-H-i-s')."-".$imageName;
        $destinationPath = $request['part'];

        $image->move($destinationPath, $newImageName);
        $data = [
            'name' => $newImageName,
            'part' => $destinationPath."/". $newImageName,
        ];
        return $this->sendResponse($data);
    }

    public function removeFile(Request $request)
    {
        $part = $request['part'];
        $fileName = $request['filename'];
        $pathFile = $part."/".$fileName;
        if(file_exists(public_path($pathFile))){
            unlink($pathFile);
            $data = ['name' => $fileName];
            return $this->sendResponse($data);
        }
    }


    public function imagePost(Request $request) {
        $thumbnailsPart = $request['thumbnailsPart'];
        $validate = $request['validate'];

        $this->validate($request, [
            'image' => $validate,
        ]);

        $image = $request->file('image');

        $imageName = time().'.'.$image->getClientOriginalExtension();

        $newImageName = date('Y-m-d-H-i-s')."-".$imageName;

        $destinationPath = $thumbnailsPart;

        $image->move($destinationPath, $newImageName);

        $data = [
            'name' => $newImageName,
            'part' => $destinationPath."/". $newImageName,
        ];
        return $this->sendResponse($data);
    }

    public function resizeImagePost(Request $request)
    {
        $thumbnailsPart = $request['thumbnailsPart'];
        $originalPart = $request['originalPart'];
        $validate = $request['validate'];

        $this->validate($request, [
            'image' => $validate,
        ]);

        $image = $request->file('image');
        $imageName = time().'.'.$image->extension();

        $newImageName = date('Y-m-d-H-i-s')."-".$imageName;

        $img = \Image::make($image->path());
        $img->resize(414, 414, function ($constraint) {
            $constraint->aspectRatio();
        })->save($thumbnailsPart.'/'.$newImageName);

        $image->move($originalPart, $newImageName);
        $data = [
            'name' => $newImageName,
            'part_original' => $originalPart."/". $newImageName,
            'part_thumbnail' => $thumbnailsPart."/". $newImageName
        ];
        return $this->sendResponse($data);
    }

    public function removeImage(Request $request)
    {
        $fileName = $request['image'];
        $path_original = "images/contents/Original/".$fileName;
        $path_thumbnail = "images/thumbnails/".$fileName;

        if(file_exists(public_path($path_original))){
            unlink($path_original);
            unlink($path_thumbnail);
            $data = ['name' => $fileName];
            return $this->sendResponse($data);
        }
    }

}
