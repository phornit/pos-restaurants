<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\Currency;
use App\Models\Backend\ExchangeRate;
use Illuminate\Http\Request;

class ExchangeRateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'exchange_rate' => 'required',
        ]);

        $exchangeRate = ExchangeRate::find($id);

        $data = $exchangeRate->update([
            'exchange_rate' => $request['exchange_rate']
        ]);

        return $this->sendResponse($data);
    }

    public function getDefaultExchangeRate(){
        $defaultCurrency = Currency::where('isDefault',1)->first();
        $exchangeRate = ExchangeRate::where('from_currency', $defaultCurrency->id)->first();
        return $this->sendResponse($exchangeRate);
    }
}
