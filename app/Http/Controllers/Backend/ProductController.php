<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\productCollection;
use App\Http\Resources\productResource;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\Product;
use App\Models\Backend\ProductPrice;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::config('Pagination');
        $data = Product::orderBy('id','desc')->paginate($num);
        return new productCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'code' => 'required','unique:tbl_product'
        ]);
        $data = Product::create($request->all());

        if ($data){
            foreach ($request['product_price'] as $value){
                if($value['price'] != "") {
                    ProductPrice::create([
                        'product_id' => $data->id,
                        'product_size_id' => $value['product_size_id'],
                        'price' => $value['price'],
                    ]);
                }
            }
        }

        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Product::find($id);
        return new productResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'code' => 'required','unique:tbl_product'
        ]);

        $product = Product::find($id);
        $data = $product->update($request->all());

        if ($data){
            ProductPrice::where('product_id', $id)->delete();

            foreach ($request['product_price'] as $value){
                if($value['price'] != "") {
                    ProductPrice::create([
                        'product_id' => $id,
                        'product_size_id' => $value['product_size_id'],
                        'price' => $value['price'],
                    ]);
                }
            }
        }

        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Product::find($id);
        $data->delete();
        return $this->sendResponse($data);
    }
}
