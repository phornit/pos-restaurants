<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Auth\UserLog;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {
        $data['url'] = \Request::get('url');
        $data['remember'] = Cookie::get('remember');
        $data['email'] = Cookie::get('email');
        $data['password'] = Cookie::get('password');
        return view('auth/login', $data);
    }

    public function authenticate(Request $request)
    {
        $url = $request['url'];
        $input = $request->all();

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';
        if(auth()->attempt(array($fieldType => Str::lower($input['username']), 'password' => $input['password'])))
        {
            UserLog::Login(auth()->user()->id);
            return redirect()->intended($url);
        }else{
            return Redirect::to('/login')
                ->with([
                    'message' => '<div class="col-md-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            អ៊ីម៉ែល ឬពាក្យសម្ងាត់មិនត្រឹមត្រូវទេ! សូមព្យាយាមម្តងទៀត។</div>
                    </div>',
                ]);
        }
    }

    public function authenticate1(Request $request)
    {
        $url = $request['url'];

        $credentials = array(
            'email' => strtolower($request['email']),
            'password' => $request['password'],
            'state' => 1
        );
        if (Auth::attempt($credentials)) {
            $remember = $request['remember'];
            if (!empty($remember)){
                $user = User::find(Auth::user()->id);
                Auth::login($user, true);
                Cookie::queue('remember', true, 50);
                Cookie::queue('email', $request['email'], 50);
                Cookie::queue('password', $request['password'], 50);
            }

                Cookie::queue('remember', false, 50);

            if ($url){
                return redirect()->intended($url);
            }else{
                return redirect()->intended('/');
            }

        }else{
            return Redirect::to('/login')
                ->with([
                    'message' => '<div class="col-md-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            អ៊ីម៉ែល ឬពាក្យសម្ងាត់មិនត្រឹមត្រូវទេ! សូមព្យាយាមម្តងទៀត។</div>
                    </div>',
                ]);
        }
    }

    public function logout(Request $request){
        UserLog::Logout(auth()->user()->id);
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return $this->loggedOut($request) ?: redirect('/');
    }
}
