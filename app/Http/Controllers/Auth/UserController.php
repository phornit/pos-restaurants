<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libraries\Backend\CoreFunction;
use App\Models\Auth\UserProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::where('id','>', 1)->paginate(CoreFunction::config('Pagination'));
        return $this->sendListResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:core_users_account',
            'password' => 'required|string|min:6',
            'user_role' => 'required|integer'
        ]);

        $data = User::create([
            'name' => Str::lower($request['name']),
            'email' => $request['email'],
            'user_role' => $request['user_role'],
            'state' => (!empty($request['state'])) ? 1 : 0,
            'password' => Hash::make($request['password']),
            'trashed' => false,
            'created_by' => auth()->user()->id,
        ]);

        if ($data){
            UserProfile::create([
                'user_account_id' => $data->id,
                'name_kh' => $request['name_kh'],
                'name_en' => $request['name_en'],
                'sex' => $request['sex'],
                'nationality' => $request['nationality'],
                'date_of_birth' => date("Y-m-d", strtotime($request['date_of_birth'])),
                'place_of_birth' => $request['place_of_birth'],
                'phone' => $request['phone'],
                'address' => $request['address'],
                'photo' => $request['photo'],
                'fax' => $request['fax']
            ]);
        }

        return $this->sendResponse($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('core_users_account')
            ->leftJoin('core_user_details', 'core_users_account.id', '=', 'core_user_details.user_account_id')
            ->select('core_users_account.*', 'core_user_details.*')
            ->where('core_users_account.id', '=', $id)
            ->first();

        if (!$data){
            return $this->sendInvalidResponse("Invalid id");
        }

        return $this->sendResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:core_users_account,email,'.$user->id,
            'user_role' => 'required|integer'
        ]);

        $data = $user->update([
            'name' => Str::lower($request['name']),
            'email' => $request['email'],
            'user_role' => $request['user_role'],
            'state' => (!empty($request['state'])) ? 1 : 0,
            'updated_by' => auth()->user()->id
        ]);

        if($data){
            if($request['user_account_id']){
                $userProfile = UserProfile::where('user_account_id',$request['user_account_id']);
                $userProfile->update([
                    'name_kh' => $request['name_kh'],
                    'name_en' => $request['name_en'],
                    'sex' => $request['sex'],
                    'nationality' => $request['nationality'],
                    'date_of_birth' => $request['date_of_birth'],
                    'place_of_birth' => $request['place_of_birth'],
                    'phone' => $request['phone'],
                    'address' => $request['address'],
                    'photo' => $request['photo'],
                    'fax'=> $request['fax']
                    ]);
            }else{
                UserProfile::create([
                    'user_account_id' => $id,
                    'name_kh' => $request['name_kh'],
                    'name_en' => $request['name_en'],
                    'sex' => $request['sex'],
                    'nationality' => $request['nationality'],
                    'date_of_birth' => $request['date_of_birth'],
                    'place_of_birth' => $request['place_of_birth'],
                    'phone' => $request['phone'],
                    'address' => $request['address'],
                    'photo' => $request['photo'],
                    'fax'=> $request['fax']
                ]);
            }
        }

        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $data = $user->delete();
        return $this->sendResponse($data);
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $users = User::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%")
                        ->orWhere('emails','LIKE',"%$search%");
            })->paginate(20);
        }else{
            $users = User::latest()->paginate(CoreFunction::config('Pagination'));
        }

        return $users;

    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
}
