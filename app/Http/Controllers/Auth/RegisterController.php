<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\SendMailVerifyEmail;
use App\Models\Auth\UserAccount;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::REGISTER;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:core_users_account'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $user
     * @return void
     */
    protected function create(array $user)
    {
        $result = User::create([
            'name' => $user['name'],
            'email' => $user['email'],
            'password' => Hash::make($user['password']),
            'user_role' => 4,
            'state' => false,
            'trashed' => false
        ]);

        if ($result){
            $data['name'] = $user['name'];
            $data['id'] = $result->id;
            Mail::to($user['email'])->queue(new SendMailVerifyEmail($data));

            return Redirect::to('/register')
                ->with([
                    'message' => '<div class="reg_sms_success">ារចុឈ្មោះរបស់អ្នកទទួលបានជោគជ័យ! សូមចូលទៅកាន់អ៊ីមែលរបស់អ្នកដើម្បីដំណើរការគណនីរបស់អ្នក។</div>',
                ]);
        }
    }


    protected function UserRegister(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:core_users_account'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $result = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'user_role' => 4,
            'state' => false,
            'trashed' => false
        ]);

        if ($result){
            $id = Crypt::encrypt($result->id);
            $data['name'] = $request['name'];
            $data['id'] = $id;

            Mail::to($request['email'])->queue(new SendMailVerifyEmail($result));


            return Redirect::to('/register')
                ->with([
                    'message' => '<div class="reg_sms_success">ការចុឈ្មោះរបស់អ្នកទទួលបានជោគជ័យ! សូមចូលទៅកាន់អ៊ីមែលរបស់អ្នកដើម្បីដំណើរការគណនីរបស់អ្នក។</div>',
                ]);
        }
    }

    public function verifyAccount($id){
        $user = UserAccount::where('id', $id)->update([
           'state' => 1,
        ]);

        if ($user){
            return redirect('/login');
        }
    }
}
