<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function forgotPassword(){
        return view('auth.forget_password');
    }

    public function password(Request $request){
        $user = User::whereEmail($request->email)->first();

        if ($user == null){
            return redirect()->back()->with([
                'message' => '<div class="col-md-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            មិនមានអ៊ីមែលនេះនៅក្នុងប្រព័ន្ធទេ!</div>
                    </div>',
            ]);
        }

        $id = Crypt::encrypt($user->id);

        return redirect()->back()->with([
            'message' => '<div class="col-md-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            មិនមានអ៊ីមែលនេះនៅក្នុងប្រព័ន្ធទេ!</div>
                    </div>',
        ]);

    }
}
