<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Auth\UserProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('core_users_account')
            ->leftJoin('core_user_details', 'core_users_account.id', '=', 'core_user_details.user_account_id')
            ->select('core_users_account.*', 'core_user_details.*')
            ->where('core_users_account.id', '=', auth()->user()->id)
            ->first();

        return $this->sendResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUserProfile(Request $request)
    {
        $id = $request['id'];
        dd($id);
        $userProfile = UserProfile::where('user_account_id',$id);
        $data = $userProfile->update([
            'name_kh' => $request['name_kh'],
            'name_en' => $request['name_en'],
            'sex' => $request['sex'],
            'nationality' => $request['nationality'],
            'date_of_birth' => $request['date_of_birth'],
            'place_of_birth' => $request['place_of_birth'],
            'phone' => $request['phone'],
            'address' => $request['address'],
            'photo' => 'photo',
            'fax'=> $request['fax']
        ]);
        return $this->sendResponse($data);
    }

    public function update(Request $request, $id)
    {
        $userProfile = UserProfile::where('user_account_id',$id);
        $data = $userProfile->update([
            'name_kh' => $request['name_kh'],
            'name_en' => $request['name_en'],
            'sex' => $request['sex'],
            'nationality' => $request['nationality'],
            'date_of_birth' => $request['date_of_birth'],
            'place_of_birth' => $request['place_of_birth'],
            'phone' => $request['phone'],
            'address' => $request['address'],
            'position' => $request['position'],
            'skill' => $request['skill'],
            'work_experience' => $request['work_experience'],
            'degree' => $request['degree'],
            'photo' => $request['photo'],
            'fax'=> $request['fax']
        ]);
        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
