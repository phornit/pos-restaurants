<?php

namespace App\Http\Controllers;

use App\Mail\SendMailable;
use App\Models\Backend\Content;
use App\Models\Backend\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    public function mail()
    {
        $name = 'Krunal';
        Mail::to('phornit@gmail.com')->send(new SendMailable($name));
        return 'Email was sent';
    }
}
