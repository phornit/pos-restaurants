<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\Category;
use Illuminate\Http\Request;

class CategoryFrontendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $num = CoreFunction::config('Frontend_Pagination');
        $data = Category::orderBy('id','desc')->paginate($num);
        return $this->sendListResponse($data);
    }

    public function search(){
        $num = CoreFunction::config('Frontend_Pagination');
        if ($search = \Request::get('q')) {
            $data = Category::where(function($query) use ($search){
                $query->where('cat_name','LIKE',"%$search%");
            })->paginate($num);
        }else{
            $data = Category::latest()->paginate($num);
        }

        return $this->sendListResponse($data);
    }
}
