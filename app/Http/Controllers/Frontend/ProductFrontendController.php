<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Resources\productCollection;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\Product;
use Illuminate\Http\Request;

class ProductFrontendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::config('Frontend_Pagination');
        $data = Product::orderBy('id','desc')->paginate($num);
        return new productCollection($data);
    }

    public function showProductByCategory($category_id){
        $num = CoreFunction::config('Frontend_Pagination');
        $data = Product::where('category_id', $category_id)->orderBy('id','desc')->paginate($num);
        return new productCollection($data);
    }

    public function searchProductByCategory($category_id){
        $num = CoreFunction::config('Frontend_Pagination');
        if ($search = \Request::get('q')) {
            $data = Product::where('category_id', $category_id)->where('code','LIKE',"%$search%")->orderBy('id', 'desc')->paginate($num);
        }else{
            $data = Product::where('category_id', $category_id)->orderBy('id', 'desc')->paginate($num);
        }
        return new productCollection($data);
    }

    public function search(){
        $num = CoreFunction::config('Frontend_Pagination');
        if ($search = \Request::get('q')) {
            $data = Product::where(function($query) use ($search){
                $query->where('code','LIKE',"%$search%");
            })->paginate($num);
        }else{
            $data = Product::latest()->paginate($num);
        }

        return new productCollection($data);
    }
}
