<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Resources\orderResource;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\Table;
use App\Models\Frontend\Order;
use App\Models\Frontend\OrderDetail;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::config('Frontend_List_Pagination');
        $data = Order::where('order_status','NEW')->orderBy('id','desc')->paginate($num);
        return $this->sendListResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->validate($request,[
                'table_id' => 'required'
            ]);

            $request['order_number'] = CoreFunction::invoiceNumber();
            $request['order_type'] = 'WALKED';
            $request['order_status'] = 'NEW';
            $request['currency_id'] = 2;
            $request['created_by'] = auth()->user()->id;
            $data = Order::create($request->all());
            if ($data){
                foreach ($request['product'] as $value){
                    if($value['product_name'] != "") {
                        OrderDetail::create([
                            'order_id' => $data->id,
                            'product_id' => $value['product_id'],
                            'product_name' => $value['product_name'],
                            'product_size_id' => $value['product_size_id'],
                            'size_name' => $value['size_name'],
                            'qty' => $value['qty'],
                            'dis_percent' => $value['dis_percent'],
                            'unit_price' => $value['unit_price'],
                            'is_free' => 0,
                            'is_void' => 0,
                            'amount' => $value['amount'],
                        ]);
                    }
                }
            }
            Table::updateStatusToBusy($request['table_id']);
            return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Order::find($id);
        return new orderResource($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $this->validate($request,[
                'table_id' => 'required'
            ]);

            $order = Order::find($id);
            Table::updateStatusToAvailable($order->table_id);
            $data = $order->update($request->all());

            if ($data){
                $orderDetail = OrderDetail::where('order_id', $id);
                $orderDetail->delete();
                foreach ($request['product'] as $value){
                    if($value['product_name'] != "") {
                        OrderDetail::create([
                            'order_id' => $id,
                            'product_id' => $value['product_id'],
                            'product_name' => $value['product_name'],
                            'product_size_id' => $value['product_size_id'],
                            'size_name' => $value['size_name'],
                            'qty' => $value['qty'],
                            'dis_percent' => $value['dis_percent'],
                            'unit_price' => $value['unit_price'],
                            'is_free' => 0,
                            'is_void' => 0,
                            'amount' => $value['amount'],
                        ]);
                    }
                }
            }
            Table::updateStatusToBusy($request['table_id']);
            return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getOrderByTable($table_id){
        $data = Order::where('table_id', $table_id)->where('order_status','NEW')->first();
        return new orderResource($data);
    }
}
