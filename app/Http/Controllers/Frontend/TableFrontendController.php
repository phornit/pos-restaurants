<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Libraries\Backend\CoreFunction;
use App\Models\Backend\Table;

class TableFrontendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {
        $num = CoreFunction::config('Frontend_Pagination');
        $data = Table::paginate($num);
        return $this->sendListResponse($data);
    }

    public function search(){
        $num = CoreFunction::config('Frontend_Pagination');
        if ($search = \Request::get('q')) {
            $data = Table::where(function($query) use ($search){
                $query->where('name','LIKE',"%$search%");
            })->paginate($num);
        }else{
            $data = Table::latest()->paginate($num);
        }

        return $this->sendListResponse($data);
    }
}
