<?php


namespace App\Libraries\Backend;


use App\Models\Frontend\Order;
use Carbon\Traits\Date;
use http\Message;
use Illuminate\Support\Facades\DB;

class CoreFunction
{
    static function getSlugToID ($slug, $tableName)
    {
        $query = DB::table($tableName);
        $query->where('slug', '=', $slug);
        $query->orwhere('id', '=', $slug);
        $results = $query->get();

        if ($results)
        {
            return $results[0]->id;
        }
    }

    public static function updateViews ($content_id, $tableName){
        DB::table($tableName)->where('id', $content_id)->increment('views');
    }

    static function config($name){
        $data = DB::table('configuration_info')
            ->where('is_active','=', '1')
            ->where('name','=', $name)->value('value');

        if ($name=="Pagination"){
            $data = (!empty(\Request::get('paginate'))) ? \Request::get('paginate') : $data;
        }

        return $data;
    }

    static function isActive ($id, $state, $name)
    {
        if ($state == 0)
        {
            $state_data = '<button class="btn btn-sm btn-danger" type="button" onclick="window.location.href=\''.URL::current().'/state?id='.$id.'&state=1&name='.$name.'\'"><i class="fa fa-eye-slash"></i></button>';
        }
        else
        {
            $state_data = '<button class="btn btn-sm btn-success" type="button" onclick="window.location.href=\''.URL::current().'/state?id='.$id.'&state=0&name='.$name.'\'"><i class="fa fa-eye"></i></button>';
        }

        return $state_data;
    }

    static function idToArray($id){
        $arr[] = preg_replace(array('/{/', '/}/'), "", $id)->toArray();
        return $arr;
    }

    static function build_created_data($user_id, $date)
    {
        $current_date		=	date('Y-m-d');
        $str_date			=	date('Y-m-d',strtotime($date));

        $user_name = CoreModel::getUserName($user_id);

        if ($user_name)
        {
            if($current_date==$str_date)
            {
                $created_log = date('d-M-Y H:i:s',strtotime($date)).'<br><span class="users">by:&nbsp;'.$user_name.'</span>&nbsp;&nbsp;<img border="0" src="'. url('/backend/img/new.gif') .'">';
            }
            else
            {
                $created_log = date('d-M-Y H:i:s',strtotime($date)).'<br><span class="users">by:&nbsp;'.$user_name.'</span>';
            }

            return $created_log;
        }
    }

    static function build_updated_data($user_id, $date)
    {
        $current_date		=	date('Y-m-d');
        $str_date			=	date('Y-m-d',strtotime($date));

        $user_name = CoreModel::getUserName($user_id);

        if ($user_name)
        {
            if($current_date==$str_date)
            {
                $updated_log = date('d-M-Y H:i:s',strtotime($date)).'<br><span class="users">by:&nbsp;'.$user_name.'</span>&nbsp;&nbsp;<img border="0" src="'. url('/backend/img/updated.gif') .'">';
            }
            else
            {
                $updated_log = date('d-M-Y H:i:s',strtotime($date)).'<br><span class="users">by:&nbsp;'.$user_name.'</span>';
            }

            return $updated_log;
        }
    }
    static function invoiceNumber()
    {
        $latest = Order::latest()->first();
        if (!$latest) {
            return 'INV-'.date('Ymd').'0001';
        }
        $string = preg_replace("/[^0-9\.]/", '', $latest->order_number);
        return 'INV-'. sprintf('%04d', $string+1);
    }
}
