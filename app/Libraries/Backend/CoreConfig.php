<?php


namespace App\Libraries\Backend;


class CoreConfig
{
    static function display_record()
    {
        return 10;
    }

    static function display_record_data()
    {
        return ['10','15','20','25','50','75','100','All'];
    }

    static function main_menu_trans_fields()
    {
        return ['name', 'alias'];
    }

    static function cont_cate_trans_fields()
    {
        return ['name', 'alias', 'metakey', 'metades'];
    }

    static function cont_trans_fields()
    {
        return ['title', 'alias','thumb_image', 'brief', 'description', 'metakey', 'metades'];
    }

    static function module_trans_fields()
    {
        return ['mod_title', 'mod_alias', 'metakey', 'metades'];
    }

    static function mod_contact_trans_fields()
    {
        return ['title', 'brief', 'one_title', 'one_brief', 'two_title', 'two_description', 'two_phone'];
    }

    static function home_trans_fields()
    {
        return ['title', 'slugan','title_1','title_2','title_3','build_img'];
    }

    static function footer_trans_fields()
    {
        return ['copyright', 'one_title', 'one_brief', 'two_title', 'three_title'];
    }

    static function slide_trans_fields(){
        return ['title','image','thumb'];
    }
    static function galleries_trans_fields(){
        return ['title','alias','image','thumb','youtube'];
    }
}
