<?php namespace App\Modules\Auth;
/**
 * Language Switcher Module
 *
 * @author			Taing Sunnguon - (Smith)
 * @real name		Taing Sunnguon <taingsunnguon@gmail.com>
 * @copyright		Copyright (c) 2015
 * @path			app/Modules/Frontend/Language_Switcher.php
 **/

use App\Models\Auth\Language;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use LaravelLocalization;

class Language_Switcher {

    static function build_language_switcher() {

        //list front language
        $list_language = self::getListLanguage();

        $html_language_switcher = '';

        $fallback_locale = \Config::get('app.fallback_locale');

        $url = url()->full();

        foreach($list_language as $val):

            $localization = $val->localization;
            $LocalizedURL = \LaravelLocalization::getLocalizedURL($localization);

            if ($localization == $fallback_locale):
                $transURL = str_replace($url, $url.'/'.$localization ,$LocalizedURL);
            else:
                $transURL = $LocalizedURL;
            endif;
            $html_language_switcher.= '<a href="javascript:void(0);" onclick="window.location.href = \''.url('/lang-frontend?url='.urldecode($transURL).'&locale='.$val->localization.'').'\'"><span class="flag-xs '.$val->image.'"></span> '.$val->trans_name.'</a> | ';
        endforeach;

        return $html_language_switcher;
    }

    static function getListLanguage ()
    {
        $query = DB::table('core_languages');
        $query->where('core_languages.state', '=', 1);
        $list_language = $query->get();

        return $list_language;
    }
}
?>
