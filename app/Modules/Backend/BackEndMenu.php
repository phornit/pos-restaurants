<?php


namespace App\Modules\Backend;


use App\Models\Backend\AdminMenu;
use App\Models\Backend\UserPermission;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BackEndMenu
{
    static function getUserRole($user_id)
    {
        $data = User::findOrFail($user_id);
        return $data['user_role'];
    }

    static function getPermission($user_role)
    {
        $data = UserPermission::where('user_roles_id', $user_role)->get();
        if ($data)
        {
            foreach ($data as $value){
                $permission[] = $value['menu_id'];
            }
            return $permission;
        }
        else
        {
            return false;
        }
    }

    static function getRowParentAdminMenu($child_id)
    {
        $adminMenu = AdminMenu::findOrFail($child_id);
        return $adminMenu;
    }

    static function getAdminMenu()
    {
        $adminMenu = AdminMenu::where('state',"1")->orderBy('id', 'ASC')->get();
        return $adminMenu;
    }

    static function getParentIdBySubMenu($uri_string)
    {
        $data = AdminMenu::where('link', $uri_string)->get();
        return $data['parent_id'];
    }

    static function build_backend_menu()
    {
        //current user
        $user_id = Auth::id();

        //user roles
        $user_role = self::getUserRole(Auth::id());

        //permission
        $array_permission = self::getPermission($user_role);

        //uri string
//        $uri_string = Request::segment(1).'/'.Request::segment(2);
//        $uri_string = trim($uri_string, '/');
//
//        //set gallery active url in content
//        if ($uri_string === 'admin/galleries')
//        {
//            $uri_string = 'admin/contents';
//        }
//        if ($uri_string === 'admin/galleries_trans')
//        {
//            $uri_string = 'admin/contents_trans';
//        }
        //get parent id of active sub menu
        $parent_active = ""; // self::getParentIdBySubMenu($uri_string);

        $uri_string="";

        //get list array
        $list_admin_menu = self::getAdminMenu();

        $html_admin_menu = '';
        for ($i = 0; $i < count($list_admin_menu); $i++)
        {
            if ($list_admin_menu[$i]->parent_id == 0)
            {
                $submenu_id  = self::sub_admin_menu_id($list_admin_menu, $list_admin_menu[$i]->id, $array_permission);
                if ($submenu_id === "")
                {
                    //menu no sub
                    if(in_array($list_admin_menu[$i]->id, $array_permission))
                    {
                        $html_admin_menu.= '<li class="nav-item">';
                        $html_admin_menu.= '<router-link to="'.$list_admin_menu[$i]->link.'" class="nav-link">';
                        $html_admin_menu.= '<i class="nav-icon '.$list_admin_menu[$i]->class_name.' '.$list_admin_menu[$i]->color.'"></i>';
                        $html_admin_menu.= '<p>'.$list_admin_menu[$i]->name.'</p>';
                        $html_admin_menu.= '</router-link>';
                        $html_admin_menu.= '</li>';
                    }
                }
                else
                {
                    $html_admin_menu.='<li class="nav-item has-treeview">';
                    $html_admin_menu.='<a href="#" class="nav-link">';
                    $html_admin_menu.='<i class="nav-icon '.$list_admin_menu[$i]->class_name.' '.$list_admin_menu[$i]->color.'"></i>';
                    $html_admin_menu.='<p>'.$list_admin_menu[$i]->name.' <i class="right fa fa-angle-left"></i></p>';
                    $html_admin_menu.='</a>';

                    //menu with sub
                    $html_admin_menu.= '<ul class="nav nav-treeview">';

                    //do sub page
                    foreach ($list_admin_menu as $val_sub)
                    {
                        if ($val_sub->parent_id == $list_admin_menu[$i]->id)
                        {
                            if(in_array($val_sub->id, $array_permission))
                            {
                                $html_admin_menu.= '<li class="nav-item">';
                                $html_admin_menu.= '<router-link to="'.$val_sub->link.'" class="nav-link">';
                                $html_admin_menu.= '<i class="'.$val_sub->class_name.' sub-nav-icon"></i> ';
                                $html_admin_menu.= '<p>'.$val_sub->name.'</p>';
                                $html_admin_menu.= '</router-link>';
                                $html_admin_menu.= '</li>';
                            }
                        }
                    }
                    $html_admin_menu.= '</ul></li>';
                }
            }
        }

        return $html_admin_menu;
    }

    static function sub_admin_menu_id($list_admin_menu, $menu_id, $array_permission)
    {
        $parent = '';
        foreach ($list_admin_menu as $val)
        {
            if ($val->parent_id == $menu_id){
                if(in_array($val->id, $array_permission))
                {
                    $parent = $val->parent_id;
                }
            }
        }
        return $parent;
    }
}
