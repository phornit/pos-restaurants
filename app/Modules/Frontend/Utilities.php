<?php


namespace App\Modules\Frontend;

use App\Models\Backend\Currency;
use App\Models\Backend\ExchangeRate;

class Utilities
{
    static function DefaultExchangeRate(){
        $defaultCurrency = Currency::where('isDefault',1)->first();
        $result = ExchangeRate::where('from_currency', $defaultCurrency->id)->pluck('exchange_rate')->first();
        return $result;
    }

    static function currentRole(){

    }
}
