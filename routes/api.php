<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Auth
Route::apiResources(['users' => 'Auth\UserController']);
Route::apiResources(['role' => 'Auth\RoleController']);
Route::apiResources(['userProfile' => 'Auth\UserProfileController']);
Route::post('updateUserProfile' , 'Auth\UserProfileController@updateUserProfile');
Route::get('audits', 'Auth\AuditController@index');
Route::get('findUser', 'Auth\UserController@search');

//Menu
Route::apiResources(['mainMenu' => 'Backend\MainMenuController']);
Route::apiResources(['menus' => 'Backend\MenuController']);
Route::put('trashed/{id}', 'Backend\MenuController@trashed');
Route::get('getParentMenu', 'Backend\MenuController@getParentMenu');
Route::get('getSubParentMenu/{id}', 'Backend\MenuController@getSubParentMenu');

//CoreBackEnd
Route::post('fileUpload', 'Backend\FileUploadController@fileUpload');
Route::post('deleteImg', 'Backend\FileUploadController@removeImage');
Route::post('deleteFile', 'Backend\FileUploadController@removeFile');
Route::post('resizeImagePost', 'Backend\FileUploadController@resizeImagePost');
Route::post('imagePost', 'Backend\FileUploadController@imagePost');

//Backend
Route::apiResources(['company' => 'Backend\CompanyController']);
Route::apiResources(['table' => 'Backend\TableController']);
Route::apiResources(['category' => 'Backend\CategoryController']);
Route::apiResources(['product-size' => 'Backend\ProductSizeController']);
Route::apiResources(['product' => 'Backend\ProductController']);

Route::apiResources(['exchangeRate' => 'Backend\ExchangeRateController']);
Route::get('getDefaultExchangeRate', 'Backend\ExchangeRateController@getDefaultExchangeRate');

//Frontend
Route::apiResources(['all-product' => 'Frontend\ProductFrontendController']);
Route::get('showProductByCategory/{id}', 'Frontend\ProductFrontendController@showProductByCategory');
Route::apiResources(['table-frontend' => 'Frontend\TableFrontendController']);
Route::apiResources(['category-frontend' => 'Frontend\CategoryFrontendController']);

Route::apiResources(['order' => 'Frontend\OrderController']);
Route::get('getOrderByTable/{id}','Frontend\OrderController@getOrderByTable');

//Frontend Search
Route::get('findCategory','Frontend\CategoryFrontendController@search');
Route::get('findTable','Frontend\TableFrontendController@search');
Route::get('findProduct','Frontend\ProductFrontendController@search');
Route::get('findProductByCategory/{id}','Frontend\ProductFrontendController@searchProductByCategory');
