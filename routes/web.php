<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('lang-backend/{locale}', [
    'before' => 'csrf',
    'as' => 'backend_locale',
    'uses' => 'LanguageController@language_backend',
]);

Route::get('{lang}/lang-frontend/', [
    'before' => 'csrf',
    'as' => 'frontend_locale',
    'uses' => 'LanguageController@language_frontend',
]);

Route::get('lang-trans/{langid}', [
    'before' => 'csrf',
    'as' => 'trans_locale',
    'uses' => 'LanguageController@language_trans',
]);


Route::get('/login', 'Auth\LoginController@login')->name('login');

Route::get('/admin', 'AdminController@index')->name('home');
Route::get('/send/email', 'HomeController@mail');
Route::post('/userRegister', 'Auth\RegisterController@UserRegister');
Route::post('/authenticate', 'Auth\LoginController@authenticate');
Route::get('/verify-account/{id}', 'Auth\RegisterController@verifyAccount');

Route::get('/admin/dashboard', 'AdminController@index');

//Backend
Route::get('/admin/dashboard', 'AdminController@index');
Route::get('/admin/role-list', 'AdminController@index');

Route::post('/reset_password_with_token', 'Auth\AccountsController@validatePasswordRequest');
Route::post('/reset_my_password', 'Auth\AccountsController@resetPassword');

Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');

Route::get('/admin/user-list', 'AdminController@index');
Route::get('/admin/user-add', 'AdminController@index');
Route::get('/admin/user-edit/{id}', 'AdminController@index');
Route::get('/admin/profile', 'AdminController@index');

Route::get('/admin/audit', 'AdminController@index');

// Restaurant
Route::get('/admin/company', 'AdminController@index');
Route::get('/admin/table', 'AdminController@index');
Route::get('/admin/category', 'AdminController@index');
Route::get('/admin/product-size', 'AdminController@index');
Route::get('/admin/product', 'AdminController@index');

//ops
Route::get('/', 'HomeController@index');
Route::get('/product-by-category/{id}', 'HomeController@index');
Route::get('/all-product', 'HomeController@index');
Route::get('/category-frontend', 'HomeController@index');
Route::get('/order-list-frontend', 'HomeController@index');
Route::get('/receipt/{id}', 'HomeController@index');

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function(){

    });

